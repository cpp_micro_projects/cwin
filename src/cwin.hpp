#pragma once
#include <vector>
#include <complex>
using namespace std;
enum class CWType {rect, hamming, hann, blackman};
class CWin
{
public:
	CWin() = delete;
	CWin(size_t n, CWType type);
	~CWin(){}
	CWin operator*(const CWin rhl);
	size_t _n;
	CWType _type;
	vector<complex<double>> _coefs;
private:
	void calc_rect();
	void calc_hamming();
	void calc_hann();
	void calc_blackman();
};