#include "cwin.hpp"
#include <algorithm>
constexpr  double pi = 3.141592;
CWin::CWin( size_t n, CWType type)
{
	_n = n;
	_type = type;
	if (_n > 0)
	{
		switch (_type)
		{
		case CWType::rect: calc_rect(); break;
		case CWType::hamming: calc_hamming(); break;
		case CWType::hann: calc_hann(); break;
		case CWType::blackman: calc_blackman(); break;
		default:
			break;
		}
	}
	
}
CWin CWin::operator*(const CWin rhl)
{
	CWin lwin(_n, _type);
	for (size_t idx=0;idx<this->_coefs.size(); ++idx)
	{
		complex<double> rval;
		rval = this->_coefs.at(idx)*rhl._coefs.at(idx);
		lwin._coefs.push_back(rval);
	}
	return lwin;
}
void CWin::calc_rect()
{
	_coefs.clear();
	fill_n(_coefs.begin(), _n, complex<double>(1.0,0.0));
}
void CWin::calc_hamming()
{
	for (size_t idx = 0; idx < _n; ++idx)
	{
		double rval = 0.54 - 0.46*cos(2 * pi*idx /(_n-1));
		_coefs.push_back(complex<double>(rval,0.0));
	}
}

void CWin::calc_hann()
{
	for (size_t idx = 0; idx < _n; ++idx)
	{
		double rval = 0.5 - 0.5*cos(2 * pi*idx /((_n - 1)));
		_coefs.push_back(complex<double>(rval, 0.0));
	}
}

void CWin::calc_blackman()
{
	for (size_t idx = 0; idx < _n; ++idx)
	{
		double rval = 0.42 - 0.5*cos(2 * pi*idx / (_n-1))+ 0.08*cos(4 * pi*idx / (_n - 1));
		_coefs.push_back(complex<double>(rval, 0.0));
	}
}
