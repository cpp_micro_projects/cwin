#include <gtest/gtest.h>
#include "../cwin.hpp"

TEST(Windows, hamming)
{
	size_t  n(5);
	CWType type =  CWType::hamming;
	CWin hamming_win(n, type);
	vector<complex<double>> exp_win{ 0.08,0.54,1.0,0.54,0.08};
	for (size_t idx = 0;idx<exp_win.size();++idx)
	{
		auto cv = hamming_win._coefs.at(idx);
		auto ev = exp_win.at(idx);
		auto diff = abs(cv - ev);
		ASSERT_LE(diff, 0.0001);

	}
}

TEST(Windows, hann)
{
	size_t  n(5);
	CWType type = CWType::hann;
	CWin win(n, type);
	vector<complex<double>> exp_win{ 0.0,0.5,1.0,0.5,0.0 };
	for (size_t idx = 0; idx < exp_win.size(); ++idx)
	{
		auto cv = win._coefs.at(idx);
		auto ev = exp_win.at(idx);
		auto diff = abs(cv - ev);
		ASSERT_LE(diff, 0.0001);

	}
}

TEST(Windows,blackman)
{
	size_t  n(5);
	CWType type = CWType::blackman;
	CWin win(n, type);
	vector<complex<double>> exp_win{ 0.0,0.34,1.0,0.34,0.0 };
	for (size_t idx = 0; idx < exp_win.size(); ++idx)
	{
		auto cv = win._coefs.at(idx);
		auto ev = exp_win.at(idx);
		auto diff = abs(cv - ev);
		ASSERT_LE(diff, 0.0001);

	}
}